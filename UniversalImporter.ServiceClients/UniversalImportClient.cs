﻿namespace Promidata.UniversalImporter.ServiceClients
{
    using System;
    using System.Collections.Generic;
    using ServiceContracts;

    public class UniversalImportClient : IUniversalImportServiceV1
    {
        private readonly IUniversalImportServiceV1 _service;

        #region Implementation of IUniversalImportServiceV1

        public UniversalImportClient(IUniversalImportServiceV1 service)
        {
            _service = service;
        }

        public ImportResultV1 Import(ImportDataV1 importData)
        {
            return _service.Import(importData);
        }

        public ImportResultV1 Update(ImportDataV1 importData)
        {
            return _service.Update(importData);
        }

        public ExistsResultV1 Exists(ExistsArgumentV1 argument)
        {
            return _service.Exists(argument);
        }

        #endregion
    }
}
