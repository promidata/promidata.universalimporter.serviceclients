﻿
namespace UniversalImporter.ServiceClients.Tests
{
    using System;
    using NUnit.Framework;
    using Promidata.UniversalImporter.ServiceContracts;
    using Promidata.UniversalImporter.Web.Services;

    [TestFixture]
    public class UniversalImportClientTests
    {
        [Test]
        public void Test001()
        {
            var uri = new Uri("net.tcp://localhost:3343/UniversalImportService.svc");
            var targetObjectAdapterFactory = new SimpleImporterFactory();
            //var universalImportServiceV1 = new UniversalImportServiceV1(targetObjectAdapterFactory);
            //UniversalImportClient client = new UniversalImportClient(universalImportServiceV1);
            var existsArgumentV1 = new ExistsArgumentV1();
            existsArgumentV1.ExternalId = "SupplierReference";
            existsArgumentV1.DatabaseIdentifier = Guid.Empty; // Die DatabaseId
            existsArgumentV1.ImporterId = new Guid("D987F41A-77FC-414A-8211-AFAD811DC03F");   // Welcher Importer?  Der SupplierImporter überprüft Supplier, usw..
            //var existsResultV1 = client.Exists(existsArgumentV1);
            //var exists = existsResultV1.Exists;
        }
    }

}
